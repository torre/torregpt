FROM python:3.10-slim

# Set the voice language
ARG VOICE_LANGUAGE=en

RUN apt update && apt install -y ffmpeg wget libespeak1
WORKDIR /app

COPY ./entrypoint.sh /app
RUN chmod +x /app/entrypoint.sh

COPY ./piper /app/piper

COPY ./main.py /app
COPY ./database.py /app
COPY ./requirements.txt /app

RUN mkdir db_data

RUN pip install --upgrade pip
RUN pip install -r requirements.txt
ENTRYPOINT [ "/app/entrypoint.sh" ]