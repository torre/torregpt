#!/bin/bash

echo "Installing piper for text to voice conversion..."
bash /app/piper/get-piper.sh

echo "Bot starting..."
python3 -u /app/main.py