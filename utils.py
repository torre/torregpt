import subprocess
import tempfile
import os

def text_to_speech(text: str) -> str:
    binary_path = "./piper"
    model_path = "blizzard_lessac-medium.onnx"
    
    # Generate a unique temporary filename
    with tempfile.NamedTemporaryFile(delete=False) as tmp:
        tmp_filename = tmp.name
    
    # Construct the command to execute the binary
    cmd = f"echo '{text}' | {binary_path} --model {model_path} --output_file {tmp_filename}"
    
    # Run the binary and wait for it to finish
    subprocess.run(cmd, shell=True, check=True)
    
    # Return the temporary filename
    return tmp_filename
