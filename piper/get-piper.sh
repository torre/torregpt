#!/bin/bash

source .env

if [ "$ENABLE_TTS" = 1 ]; then
    echo "Installing piper for text to voice conversion..."
    echo "Downloading piper v0.0.2.."
    wget -q https://github.com/rhasspy/piper/releases/download/v0.0.2/piper_amd64.tar.gz && \
    echo "Extracting piper" && \
    tar -xf piper_amd64.tar.gz && \
    rm piper_amd64.tar.gz && \
    chmod -R 777 ./piper/ && \
    mkdir piper/voices

    # Download voices for all or selected languages
    if [[ "$VOICE_LANGUAGE_LIST" == "*" ]]; then
        langs=( "en" "es" "fr" "it" "pt" "ca" "de" "nl" "no" )
    else
        IFS=',' read -r -a langs <<< "$VOICE_LANGUAGE_LIST"
    fi

    echo "Downloading tts voices from VOICE_LANGUAGE_LIST..."
    echo "This can take a while..."

    for lang in "${langs[@]}"; do
        case $lang in
            "en" )
                voice_file="voice-en-us-ryan-high.tar.gz"
                ;;
            "es" )
                voice_file="voice-es-mls_9972-low.tar.gz"
                ;;
            "fr" )
                voice_file="voice-fr-siwis-medium.tar.gz"
                ;;
            "it" )
                voice_file="voice-it-riccardo_fasol-x-low.tar.gz"
                ;;
            "pt" )
                voice_file="voice-pt-br-edresson-low.tar.gz"
                ;;
            "ca" )
                voice_file="voice-ca-upc_ona-x-low.tar.gz"
                ;;
            "de" )
                voice_file="voice-de-thorsten-low.tar.gz"
                ;;
            "nl" )
                voice_file="voice-nl-rdh-medium.tar.gz"
                ;;
            "no" )
                voice_file="voice-no-talesyntese-medium.tar.gz"
                ;;
            * )
                echo "Ignoring unrecognized language code: $lang"
                continue
                ;;
        esac

        echo "Downloading $lang voice..."
        wget -q https://github.com/rhasspy/piper/releases/download/v0.0.2/$voice_file && \
        tar -xf $voice_file && \
        rm $voice_file && \
        mv $lang-*.onnx piper/voices/$lang.onnx && \
        mv $lang-*.onnx.json piper/voices/$lang.onnx.json

        echo "Done"
    done

    echo "Done. Piper installed!"
else
    echo "TTS Disabled. No work to do..."
fi
